Theme Name: Codium
Description: Codium is a wordpress (3.9 compatible) theme simple and minimalist but fully customizable: change the background color, insert a header image, incorporating a background image and adding automatic post thumbnails on the homepage. Play with colors and change the theme as you want! For french wordpress users, this theme is available in french too! Bonus : this theme is optimized for handheld and mobile devices (resize the windows and you will see). Pour les français ce thème est fourni avec une localisation en français!
Author: Henri Labarre
Theme URI: http://codium.code-2-reduction.fr/
Author URI: https://twitter.com/henrilabarre
Version: 1.3
Tags: custom-colors, one-column, fixed-layout, custom-background, custom-header, threaded-comments, sticky-post, light, white, custom-background, translation-ready,custom-menu, editor-style, featured-images, responsive-layout, microformats
License: GNU General Public License v2.0
License URI: http://www.gnu.org/licenses/gpl-2.0.html


Changes :

1.0.9 : some small changes to be compliant to wordpress 3.2
1.1 : compatibility to wordpress 3.4
1.3 : compatibility to wordpress 3.9 and 4 beta
