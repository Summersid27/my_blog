<?php

add_action('wp_enqueue_scripts', 'my_theme_styles' );
function my_theme_styles() {
	wp_enqueue_style('parent-theme-css', get_template_directory_uri() .'/style.css' );
	wp_enqueue_style('child-theme-css', get_stylesheet_directory_uri() .'/style.css', array('parent-theme-css') );
}

function copyright() {
	$head = '<! --Summersid, blackspase@gmail.com. All rights reserved--> ';
}
add_action('wp_head', 'copyright');

add_action('wp_footer', 'footer_copyright');
function footer_copyright() {
	$content = '<div id="footerlink"><div class="center"><p>© «Summersid», 2017. «Все права защищены».</p></div><div class="clear"></div></div></div>';
	print $content;
}